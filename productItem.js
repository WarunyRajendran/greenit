export const suvItem = [
    {
        id: 1,
        name: 'Jaguar E-Pace',
        price: 45600,
        image: 'https://picsum.photos/200/300',
    },
    {
        id: 2,
        name: 'Jaguar F-Pace',
        price: 50230,
        image: 'https://picsum.photos/200/300',
    },
    {
        id: 3,
        name: 'Jaguar I-Pace',
        price: 60000,
        image: 'https://picsum.photos/200/300',
    }
]

export const breakItem = [
    {
        id: 1,
        name: 'Jaguar XE',
        price: 47700,
        image: 'https://picsum.photos/200/300',
    },
    {
        id: 2,
        name: 'Jaguar XF',
        price: 50130,
        image: 'https://picsum.photos/200/300',
    }
]

export const sportItem = [
    {
        id: 1,
        name: 'Jaguar F-Type',
        price: 65000,
        image: 'https://picsum.photos/200/300',
    }
]