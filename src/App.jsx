import {
  BrowserRouter as Router,
  Route,
  Routes,
} from "react-router-dom";
import HomePage from './components/pages/Home/home';
import SVO from './components/pages/SVO/index';
import Guides from './components/pages/Guides/index';
import DetailProduct from './components/pages/detailProduct/detailProduct'
import Epace from './components/pages/categories/ePace'
import Fpace from './components/pages/categories/fPace'
import Ipace from './components/pages/categories/iPace'
import XE from './components/pages/categories/xe'
import Ftype from './components/pages/categories/fType'
import XF from './components/pages/categories/xf'

function App() {

  return (
    <Router>
      <Routes>
        <Route path="/" element={<HomePage />}/>
        <Route path="/svo" element={<SVO />}/>
        <Route path="/guides" element={<Guides/>}/>
        <Route path="/cars/:id" element={<DetailProduct />}/>
        <Route path="/e-pace" element={<Epace />}/>
        <Route path="/f-pace" element={<Fpace />}/>
        <Route path="/i-pace" element={<Ipace />}/>
        <Route path="/xe" element={<XE />}/>
        <Route path="/f-type" element={<Ftype />}/>
        <Route path="/xf" element={<XF />}/>
      </Routes>
    </Router>
  )
}

export default App
