import React, { useState } from 'react';
import { useParams, useLocation, Link } from 'react-router-dom';
import './index.css';

const Product = () => {
  const [confirmationOrder, setconfirmationOrder] = useState("");
  const location = useLocation();
  const { from } = location.state

  const itemName  = [
    "Design",
    "Intérieur",
    "Performances",
    "Confort",
  ]

  const renderNavLink = (content) => {

    const scrollToId = content.toLowerCase();

    const handleClick = () => {
      document.getElementById(scrollToId).scrollIntoView({ behavior: 'smooth' });
    }

    return (
      <div key={content} className="product__menu__item">
        <p className="product__menu__item__text" onClick={handleClick} style={{backgroundColor: "transparent"}}>{content}</p>
      </div>
    )
  }

  const openModal = (e) => {
    setconfirmationOrder("");
    document.getElementById("myModal").style.display = "block";
  }

  const closeModal = (e) => {
    document.getElementById("myModal").style.display = "none";
  }

  const orderConfirmation = (e) => {
    setconfirmationOrder("Votre commande a bien été prise en compte :)");
  }


  return (
    <>
      <section className="product-header">
        <h1>{from.model}</h1>
        <div className="presentation-jaguar">
          <div className="b-eco">
            <div>
              <picture>
                <source srcSet="/product/eco-fuel.avif" media="(max-width: 768px)"/>
                <img src="/product/eco-fuel.avif" alt=""/>
              </picture>
            </div>
            <div className="b-eco-icon-fuel">
              <span className="b-eco__value">{from.consumption}</span>
              <div className="b-eco__title">l/100 km</div>
            </div>

            <div className="eff_main">
              <div className="eff_container">
                <div className="eff_arrow eff_A">
                  <span className="eff_label">A</span>
                </div>
                <div className="eff_arrow eff_B">
                  <span className="eff_label">B</span>
                </div>
                <div className="eff_arrow eff_C">
                  <span className="eff_label">C</span>
                </div>
                <div className="eff_arrow eff_D">
                  <span className="eff_label">D</span>
                </div>
                <div className="eff_arrow eff_E">
                  <span className="eff_label">E</span>
                </div>
                <div className="eff_arrow eff_F eff_active">
                  <span className="eff_label eff--large">F</span>
                  <span className="eff_emission">220 gCO₂/km</span>
                </div>
                <div className="eff_arrow eff_G">
                  <span className="eff_label">G</span>
                </div>
              </div>
            </div>
          </div>
          <div>
            <picture>
              <source srcSet={from.image_presentation} media="(max-width: 768px)"/>
              <img src={from.image_presentation} className="presentation-jaguar-img" loading="lazy" alt="image_presentation"/>
            </picture>
          </div>
        </div>

        <div className="m-364-techspecs-center">
          <div className="m-364-module-specs">
            <div className="m-364-module-specs-data">
              <div className="m-364-module-specs-data--title">220 kW/{from.horsepower} ch</div>
              <div className="m-364-module-specs-data--copy">Puissance (kW)/Puissance (ch)</div>
            </div>
            <div className="m-364-module-specs-data">
              <div className="m-364-module-specs-data--title">{from.acceleration} s</div>
              <div className="m-364-module-specs-data--copy">Accélération de 0 à 100 km/h</div>
            </div>
            <div className="m-364-module-specs-data">
              <div className="m-364-module-specs-data--title">{from.top_speed} km/h</div>
              <div className="m-364-module-specs-data--copy">Vitesse maximale sur circuit</div>
            </div>
          </div>
        </div>
      </section>

      <section className="product-data">
        <div className="product-menu-middle">
          {itemName.map(nav => renderNavLink(nav))}
        </div>

        <div className='product-caracteristique' id="design">
          <div>
            {from.image_design ?
              <picture>
                <source srcSet={from.image_design} media="(max-width: 768px)"/>
                <img src={from.image_design} alt="image_design" loading="lazy"/>
              </picture>
            :
              <video controls width="850" height="670" style={{marginLeft: "2%"}} src={from.video_design} title="Nouvelle Jaguar E-Pace p200 Mhev Awd Flexfuel" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowFullScreen></video>
            }
          </div>
          <div className='product-carac-texte'>
            <h2>Design</h2>
            {from.designs.map((item, index) => {
              return <p key={index}>{item.design}</p>
            })}
          </div>
        </div>

        <div className='product-caracteristique' id="intérieur">
          <div className='product-carac-texte'>
            <h2>Intérieur</h2>
            {from.insides.map((item, index) => {
              return <p key={index}>{item.inside}</p>
            })}
          </div>
          <div className="jaguar-image-inside">
            <picture>
              <source srcSet={from.image_inside} media="(max-width: 768px)"/>
              <img src={from.image_inside} alt="image_inside" loading="lazy"/>
            </picture>
          </div>
        </div>

        <div className='product-caracteristique' id="performances">
          <div className='product-carac-texte'>
            <h2>Performance</h2>
            {from.performances.map((item, index) => {
              return <p key={index}>{item.performance}</p>
            })}
          </div>
        </div>

        <div className='product-caracteristique' id="confort">
          <div className="jaguar-image-comfort">
            <picture>
              <source srcSet={from.image_comfort} media="(max-width: 768px)"/>
              <img src={from.image_comfort} alt="image_comfort" loading="lazy"/>
            </picture>
          </div>
          <div className='product-carac-texte'>
            <h2>Confort</h2>
            {from.comforts.map((item, index) => {
              return <p key={index}>{item.comfort}</p>
            })}
          </div>
        </div>

        <div className="product-price">
          <div>
            <p>À partir de {from.price} €</p>
          </div>
          <div>
            <button className="order-button" onClick={openModal}>Commander</button>
          </div>
        </div>
        <div className="return-list-div">
          <Link className="return-model-list" to={`/${from.category}`}>&#32; Retour à la categorie {from.category}</Link>
        </div>
      </section>

      <div id="myModal" className="modal">
        <div className="modal-content">
          <span className="close" onClick={closeModal}>&times;</span>
          <h2 className="modal-title">Commander la {from.model}</h2>
          <p className="modal-car-price">À partir de : <strong>{from.price}</strong></p>
          <div className="form-cb-data">
            <h3>Type de carte bancaire</h3>
            <div>
              <input id="visa" name="type_de_carte" type="radio" />
              <label htmlFor="visa">&#32;VISA</label>
            </div>
            <div>
              <input id="amex" name="type_de_carte" type="radio" />
              <label htmlFor="amex">&#32;AmEx</label>
            </div>
            <div>
              <input id="mastercard" name="type_de_carte" type="radio" />
              <label htmlFor="mastercard">&#32;Mastercard</label>
            </div>
            <div>
              <label htmlFor="numero_de_carte">N° de carte&#32;</label>
              <input id="numero_de_carte" name="numero_de_carte" type="number" required />
            </div>
            <div>
              <label htmlFor="securite">Code sécurité&#32;</label>
              <input id="securite" name="securite" type="number" required />
            </div>
            <div>
              <label htmlFor="nom_porteur">Nom du porteur&#32;</label>
              <input id="nom_porteur" name="nom_porteur" type="text" placeholder="Même nom que sur la carte" required />
            </div>
          </div>
          <button className="modal-order-car" onClick={orderConfirmation}>Valider</button>
          &#32;
          {confirmationOrder}
        </div>
      </div>
  </>
  )
}

export default Product