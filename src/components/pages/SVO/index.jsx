import Template from '../../template/index';
import { Link } from 'react-router-dom';
import { data, xeSvProject8 } from './data';
import './index.css';

const SVO = () => {

    return (
        <Template>
            <div className="svo">
                <div className="svo-intro">
                    <div className="svo-intro-title">
                        <img src="./svo/svo-logo.avif" loading="lazy"/>
                        <h1>Special Vehicule Operations</h1>
                    </div>
                    <p>
                        Des performances exceptionnelles, un luxe inégalé et une technologie de pointe. 
                        La division Special Vehicle Operations présente le meilleur de Jaguar avec une gamme 
                        exclusive de modèles phares estampillés SV, des séries limitées exclusives ainsi que 
                        des projets sur mesure.
                    </p>
                </div>

                <div className="svo-vehicles">
                    <h1>Nos véhicules spéciaux</h1>
                    <div className="svo-vehicles-cards">
                        {data.map((data, key) => {
                            return (
                                <div className="" key={key}>
                                    <img src={data.img} loading="lazy"/>
                                    <p className="svo-vehicles-cards-title">{data.title}</p>
                                    <p className="svo-vehicles-cards-description">{data.description}</p>
                                    <Link to={`/cars/${data.id}`} state={{ from: data }} className="svo-vehicles-cards-link">&#x3E; {data.linkDescription}</Link>
                                </div>
                            )
                        })}
                    </div>
                </div>

                <div className="svo-granTurismo">
                    <div className="svo-granTurismo-text">
                        <h1>XE SV PROJECT 8</h1>
                        <p>La plus extrême des Jaguars</p>
                        <p>
                            Avec son moteur V8 Supercharged légendaire, la Project 8 a été conçue pour être
                            l'une des berlines sportives les plus performantes au monde, mais sa vitesse ne 
                            repose pas seulement sur une puissance exceptionnelle. Avec son aérodynamique inspirée des
                            sports automobiles, son utilisation extensive du carbone et une carrosserie légère en aluminium, 
                            aucune autre Jaguar ne permet de se sentir aussi vivant sur la route.
                        </p>
                        <Link to={`/cars/${xeSvProject8[0].id}`} state={{ from: xeSvProject8[0]}}>En savoir plus</Link>
                    </div>
                        <img src="./svo/svo-xe-proj8.avif" loading="lazy"/>
                </div>

                <div className="svo-installations">
                    <div className="svo-installations-content">
                        <img src="./svo/svo-installations.avif" loading="lazy"/>
                        <div>
                            <h1>Nos Installations</h1>
                            <p>
                                La division Special Vehicle Operations dispose d’un centre technique de 20 M £, 
                                situé à Warwickshire en Angleterre. Ce site de production spécialisé a été fondé par 
                                Jaguar Land Rover et accueille une équipe de 200 spécialistes automobiles. 
                                S'étendant sur 20 000 m2, le siège social de SVO dispose de plus de 40 zones de contrôle 
                                qualité et d'un atelier de peinture de pointe.
                            </p>
                        </div>
                    </div>
                </div>

                <div className="svo-instagram">
                    <div className="svo-instagram-text">
                        <h1>SV sur instagram</h1>
                        <p>
                            Suivez @JaguarSV pour découvrir les toutes 
                            dernières photos, vidéos et histoires des véhicules Jaguar les 
                            plus uniques au monde.
                        </p>
                        <a href="https://www.instagram.com/jaguarsv/?igshid=1ab7wrsarsiwd" target="_blank">Explorez l'instagram SV</a>
                    </div>
                    <img src="./svo/svo-instagram.avif" loading="lazy"/>
                </div>

            </div>
        </Template>
    )
}
  
export default SVO;
  