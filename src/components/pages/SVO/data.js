export const data = [
  {
    id: 30,
    model: 'JAGUAR F-PACE SVR EDITION 1988',
    price: 138200,
    img: './svo/svo-fpace-88.avif',
    title: 'F-PACE SVR EDITION 1988',
    description: 'Performances de luxe. Limited edition.',
    linkDescription: 'Découvrez l\'édition 1988',
    image_presentation: '../product/f-pace-svr-88/f-pace-svr-88-presentation.avif',
    image_design: '../product/f-pace-svr-88/f-pace-svr-88-design.avif',
    image_inside: '../product/f-pace-svr-88/f-pace-svr-88-inside.avif',
    image_comfort: '../product/f-pace-svr-88/f-pace-svr-88-comfort.avif',
    designs: [
      {
        design: 'La peinture Midnight Amethyst, les détails Sunset Gold et les jantes en alliage forgé 22” Champagne Gold de l’Edition 1988 rappellent les couleurs uniques de la XJR-9.'
      },
      {
        design: 'La F-PACE SVR Edition 1988 est limitée à 394 exemplaires. Un détail mis en évidence sur l’habillage du tableau de bord “SV Bespoke”.'
      }
    ],
    insides: [
      {
        inside: 'Pour mettre en valeur le plus spécial des modèles F-PACE, le “R” rouge iconique du badge SVR est remplacé par une finition noire.'
      },
      {
        inside: 'Les plaques de seuil éclairées et l’imprimé de siège arborant les logos “Edition 1988” sont un clin d’œil supplémentaire à la place qu’occupe Jaguar dans l’histoire de la course automobile.'
      },
      {
        inside: 'C\'est l’expression parfaite de l’association entre luxe moderne à couper le souffle et puissance Jaguar brute.'
      }
    ],
    performances: [
      {
        performance: 'En 1988, la Jaguar XJR-9 lancée à plus de 385 km/h a fait vibrer les fans de course. Aujourd’hui, la F-PACE SVR Edition 1988 continue de repousser les limites du possible.'
      },
      {
        performance: 'Au-delà de son moteur V8 5.0 L suralimenté de 550 ch incroyablement puissant, la F-PACE SVR Edition 1988 intègre également une multitude de fonctions sophistiquées.'
      }
    ],
    comforts: [
      {
        comfort: 'Les sièges Performance SVR chauffants et ventilés en cuir Semi-Aniline, les finitions fibre de carbone et le toit panoramique coulissant garantissent à la fois esthétisme et fonctionnalité sans pareil.'
      }
    ],
    consumption: '5.0',
    acceleration: '4',
    horsepower: '550',
    top_speed: '385',
    category: "F-Pace"
  },
  {
    id: 31,
    model: 'JAGUAR F-PACE SVR',
    price: 113160,
    img: './svo/svo-fpace-svr.avif',
    title: 'F-PACE SVR',
    description: 'Le plus performant des SUV.',
    linkDescription: 'Découvrez ce véhicule',
    link: '',
    image_presentation: '../product/f-pace-svr/f-pace-svr-presentation.avif',
    image_design: '../product/f-pace-svr/f-pace-svr-design.avif',
    image_inside: '../product/f-pace-svr/f-pace-svr-inside.avif',
    image_comfort: '../product/f-pace-svr/f-pace-svr-comfort.avif',
    designs: [
      {
        design: 'Au niveau de l\'extérieur, la F-PACE SVR présente des lignes sportives et dynamiques, avec des courbes élégantes et des détails agressifs.'
      },
      {
        design: 'Les jantes en alliage léger de 21 ou 22 pouces sont spécifiques à la F-PACE SVR et ont été conçues pour maximiser l\'aérodynamisme et améliorer la stabilité à haute vitesse.'
      }
    ],
    insides: [
      {
        inside: 'À l\'intérieur, la F-PACE SVR offre un espace spacieux et luxueux, avec des matériaux de haute qualité et une finition impeccable. Les sièges sport en cuir sont rembourrés pour offrir un excellent soutien latéral, tandis que le volant sport en cuir est équipé de palettes de changement de vitesse en aluminium.'
      },
      {
        inside: 'Le tableau de bord numérique de 12,3 pouces offre une visualisation claire et précise des informations de conduite, tandis que l\'écran tactile de 10 pouces permet un accès facile aux fonctions de divertissement et de connectivité.'
      },
      {
        inside: 'Le système de son Meridian haute performance offre une expérience d\'écoute de qualité supérieure.'
      }
    ],
    performances: [
      {
        performance: 'Elle dispose d\'un système de suspension adaptative qui offre une conduite souple et confortable sur la route, ainsi qu\'une direction précise pour une maniabilité maximale.'
      },
      {
        performance: 'La F-PACE SVR est également équipée de freins haute performance et de pneus spécifiques, ce qui lui permet de s\'arrêter rapidement et en toute sécurité.'
      },
      {
        performance: 'Les freins sont ventilés et perforés, avec des étriers de frein rouges qui offrent une excellente puissance de freinage et une réponse rapide.'
      }
    ],
    comforts: [
      {
        comfort: 'Les sièges en cuir sportifs offrent un excellent soutien latéral pour une conduite sportive, mais sont également confortables pour les trajets plus longs.'
      }
    ],
    consumption: '5.0',
    acceleration: '4',
    horsepower: '550',
    top_speed: '370',
    category: "F-Pace"
  }
];

export const xeSvProject8 = [
  {
    id: 32,
    model: 'JAGUAR XE SV PROJECT 8',
    price: 138200,
    title: 'XE SV PROJECT 8',
    image_presentation: '../product/xe-sv-project-8/xe-sv-project-8-presentation.avif',
    image_design: '../product/xe-sv-project-8/xe-sv-project-8-design.avif',
    image_inside: '../product/xe-sv-project-8/xe-sv-project-8-inside.avif',
    image_comfort: '../product/xe-sv-project-8/xe-sv-project-8-comfort.avif',
    designs: [
      {
        design: 'La carrosserie de la Project 8 a été largement repensée afin d\'offrir les performances aérodynamiques essentielles à un véhicule pouvant atteindre 320 km/h.'
      },
      {
        design: 'Cela comprend un déflecteur avant ajustable, un diffuseur arrière et un grand aileron arrière réglable. L\'angle d\'attaque de l\'aileron peut être paramétré avec précision afin de minimiser la traînée et d\'optimiser l\'appui qui peut générer une charge pouvant atteindre 122 kg lorsque la vitesse atteint 300 km/h.'
      }
    ],
    insides: [
      {
        inside: 'Pour ceux qui souhaitent bénéficier de sièges pour la piste, il est possible d\'opter pour des sièges baquets en fibre de carbone avec harnais quatre points'
      },
      {
        inside: 'En choisissant cette configuration, un arceau de protection vient remplacer les sièges arrières.'
      },
      {
        inside: 'Pour plus de polyvalence, il existe aussi une version route équipée de quatre sièges revêtus de cuir Ebony avec coutures contrastées Oyster (dont deux sièges Performance en magnésium à l’avant).'
      }
    ],
    performances: [
      {
        performance: 'Conçue pour être la berline sportive la plus exaltante au monde, la Project 8 utilise une suspension dérivée des sports automobiles ainsi qu\'un système de transmission intégrale revu afin d\'offrir au conducteur des sensations et une agilité exceptionnelles'
      },
      {
        performance: 'L\'amortissement variable continu améliore la réactivité et offre deux hauteurs de garde au sol : l\'une adaptée à la conduite sur route et l\'autre, plus basse de 15 mm, adaptée à la conduite sur circuit.'
      },
      {
        performance: 'Le légendaire moteur V8 suralimenté qui équipe la Project 8 n’est pas le seul atout dont elle bénéficie. Grâce à une structure en aluminium léger et à l\'utilisation intensive de la fibre de carbone (capot, pare-chocs, ailes, jupes latérales, prise d\'air avant et aileron arrière), la Project 8 se veut aussi extrêmement légère.'
      }
    ],
    comforts: [
      {
        comfort: 'Sièges Performance avant et arrière en cuir Semi-Aniline, nouveau volant et système d’infodivertissement intuitif Pivi Pro.'
      }
    ],
    consumption: '5.0',
    acceleration: '3,7',
    horsepower: '600',
    top_speed: '320',
    category: "XE"
  },
]