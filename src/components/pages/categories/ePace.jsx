import Template from '../../template/index';
import Card from '../../../components/card/index'
import './index.css'

const Epace = () => {
    return (
        <Template>
            <div>
                <picture className="hero-picture">
                    <source media="(min-width: 768px)" srcSet="/categories/e-pace/jaguar_hero.avif" />
                    <source media="(min-width: 576px)" srcSet="/categories/e-pace/jaguar_hero_768x256.avif" />
                    <img className="hero-img" srcSet="/categories/e-pace/jaguar_hero_600x200.avif" alt="Jaguar Car Banner" />
                </picture>
                <Card category="E-Pace"/>
            </div>
        </Template>
    )
}

export default Epace;