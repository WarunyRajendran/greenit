import Template from '../../template/index';
import Card from '../../card/index'
import './index.css'

const Ftype = () => {
    return (
        <Template>
            <div>
                <picture className="hero-picture">
                    <source media="(min-width: 768px)" srcSet="/categories/f-type/jaguar_hero.avif" />
                    <source media="(min-width: 576px)" srcSet="/categories/f-type/jaguar_hero_768x256.avif" />
                    <img className="hero-img" srcSet="/categories/f-type/jaguar_hero_600x200.avif" alt="Jaguar Car Banner" />
                </picture>
                <Card category="F-Type"/>
            </div>
        </Template>
    )
}

export default Ftype;