import Template from '../../template/index';
import Card from '../../card/index'
import './index.css'

const XF = () => {
    return (
        <Template>
            <div>
                <picture className="hero-picture">
                    <source media="(min-width: 768px)" srcSet="/categories/xf/jaguar_hero.avif" />
                    <source media="(min-width: 576px)" srcSet="/categories/xf/jaguar_hero_768x256.avif" />
                    <img className="hero-img" srcSet="/categories/xf/jaguar_hero_600x200.avif" alt="Jaguar Car Banner" />
                </picture>
                <Card category="XF"/>
            </div>
        </Template>
    )
}

export default XF;