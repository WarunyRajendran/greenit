import Template from '../../template/index';
import Card from '../../card/index'
import './index.css'

const XE = () => {
    return (
        <Template>
            <div>
                <picture className="hero-picture">
                    <source media="(min-width: 768px)" srcSet="/categories/xe/jaguar_hero.avif" />
                    <source media="(min-width: 576px)" srcSet="/categories/xe/jaguar_hero_768x256.avif" />
                    <img className="hero-img" srcSet="/categories/xe/jaguar_hero_600x200.avif" alt="Jaguar Car Banner" />
                </picture>
                <Card category="XE"/>
            </div>
        </Template>
    )
}

export default XE;