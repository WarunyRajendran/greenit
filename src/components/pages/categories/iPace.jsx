import Template from '../../template/index';
import Card from '../../card/index'
import './index.css'

const iPace = () => {
    return (
        <Template>
            <div>
                <picture className="hero-picture">
                    <source media="(min-width: 768px)" srcSet="/categories/i-pace/jaguar_hero.avif" />
                    <source media="(min-width: 576px)" srcSet="/categories/i-pace/jaguar_hero_768x256.avif" />
                    <img className="hero-img" srcSet="/categories/i-pace/jaguar_hero_600x200.avif" alt="Jaguar Car Banner" />
                </picture>
                <Card category="I-Pace"/>
            </div>
        </Template>
    )
}

export default iPace;