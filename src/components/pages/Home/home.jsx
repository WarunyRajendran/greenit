import Template from '../../template/index';
import Carousel from '../../carousel/index';
import './index.css';

const Home = () => {
  return (
    <Template>
      <Carousel/>
      <div className="homepage">
        <div className='homepage-performance'>
          <img src="./home/home-perf.avif" loading="lazy"/> 
          <div className="homepage-performance-text">
            <h1>L'art de la performance</h1>

            <p>
              La vie n’est pas seulement une question de chiffres, c’est également un ensemble de sensations, 
              d’émotions qui vous surprennent et peuvent vous laisser sans voix. 
              Ce sont pour ces moments extraordinaires que nous vivons. 
              Nos designers et ingénieurs développent des voitures aux performances intelligentes, 
              au design exceptionnel qui mettent tous vos sens en éveil et font battre votre coeur encore plus fort. 
              Nos voitures naissent de notre passion. La performance ne peut pas être mesurée, elle se ressent. 
              C’est ce que nous appelons l’art de la performance.
            </p>
          </div>
        </div>
        <div className="homepage-svo">
          <div className="homepage-svo-text">
            <h1>SVO (SPECIAL VEHICLE OPERATIONS)</h1>
            <p>
              SVO repousse les limites en offrant puissance, conduite et 
              tenue de route incomparables ; 
              luxe, choix et personnalisation inégalés.
            </p>
            <a href="/svo">En savoir plus</a>
          </div>
          <img src="./home/home-svo.avif" loading="lazy"/>
        </div>

        <div className="homepage-exp">
          <img src="./home/home-exp.avif" loading="lazy"/>
          <div className="homepage-exp-text">
            <h1>L'expérience Jaguar</h1>

            <p>
              Le meilleur moyen de connaître Jaguar est de se rapprocher de nos voitures et 
              de venir à notre rencontre. Nous participons à la plupart des grandes manifestations 
              telles que le « Goodwood Festival of Speed » et les plus grands salons du monde dont 
              le Mondial de Paris. Ce sont des événements durant lesquels nous dévoilons les dernières 
              améliorations apportées à nos différentes gammes, de la présentation 
              d'un concept car au lancement d'un modèle innovant.
            </p>
          </div>
        </div>
      </div>


    </Template>
  )
}

export default Home;