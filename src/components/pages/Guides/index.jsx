import Template from '../../template/index';
import './index.css';

const Guides = () => {

    return (
        <Template>
            <div className="guides">
                <h1 className="guides-title">Manuels et Guides</h1>
                <p className="guides-description">
                    Feuilletez les manuels digitaux, achetez la littérature de bord ou regardez 
                    les vidéos intéressantes pour utiliser votre Jaguar au maximum.
                </p>


                <div className="guides-xj">
                    <img src="./manuels/manuel-xj.avif" loading="lazy"/>

                    <div className="guides-xj-text">
                        <h1>Bibliothèque XJ</h1>
                        <p>
                            Vous préférerez sans doute conserver le manuel du conducteur et
                            le guide de prise en main de votre XJ dans la boîte à gants. 
                            Vous pouvez aussi télécharger ces documents sous forme 
                            électronique, ainsi que des brochures Jaguar, à consulter 
                            lorsque vous aurez un moment :
                        </p>
                        <div className="guides-xj-docs">
                            <a href="https://www.jaguar.fr/content/dam/jdx/pdfs/fr/JJM100221802.pdf" target="_blank">&#x3E; Juillet 2009 - Février 2011</a>
                            <a href="https://www.jaguar.fr/content/dam/jdx/pdfs/fr/12MY_FR_XJ_Owner_Handbooks_Sept_2011.pdf" target="_blank"> &#x3E; Mars 2011 - Juin 2012</a>
                        </div>
                    </div>

                </div>

                <div className="guides-xf">
                    <div className="guides-xf-text">
                        <h1>Bibliothèque XF</h1>
                        <p>Toutes les informations dont vous avez besoin sur les anciens modèles XF :</p>

                        <div className="guides-xf-docs">
                            <a href="https://www.jaguar.fr/content/dam/jdx/pdfs/fr/JJM_11_02_40_901_XF.pdf" target="_blank">&#x3E; Février 2009 - Février 2010</a>
                            <a href="https://www.jaguar.fr/content/dam/jdx/pdfs/fr/JJM_11_02_40_101_XF.pdf" target="_blank">&#x3E; Mars 2010 - Février 2011</a>
                            <a href="https://www.jaguar.fr/content/dam/jdx/pdfs/fr/12MY_FR_XF_Owner_Handbook_Sept_2011.pdf" target="_blank" className="last-link">&#x3E; Mars 2011 - Juin 2012</a>
                        </div>
                    </div>

                    <img src="./manuels/manuel-xf.avif" loading="lazy"/>
                </div>

                <div className="guides-iguide">
                    <img src="./manuels/manuel-iguide.avif" loading="lazy"/>

                    <div className="guides-iguide-text">
                        <h1>I-Guide</h1>
                        <p>
                            L'application iGuide Jaguar est une version numérique de votre 
                            manuel du conducteur. Elle vous permet de trouver rapidement et 
                            facilement les informations dont vous avez besoin concernant les 
                            fonctionnalités et les caractéristiques du véhicule.
                        </p>
                        
                        <div className="iguide-docs">
                            <a href="https://apps.apple.com/fr/app/jaguar-iguide/id1304275634" target="_blank">&#x3E; Téléchargement pour IOS</a>
                            <a href="https://play.google.com/store/apps/details?id=com.jaguar.iguideapp&pli=1" target="_blank">&#x3E; Téléchargement pour Android</a>
                        </div>

                    </div>
                </div>

            </div>
        </Template>
    )
}
  
export default Guides;
  