import Menu from '../menu/index';
import Burger from '../menu/burger/index';
import Footer from '../footer/index';

const Template = ({ children }) => {
  return (
    <>
      {(window.innerWidth > 768 ? 
        <Menu />
      : <Burger />)}

        { children }

      <Footer />
    </>
  );
};

export default Template;