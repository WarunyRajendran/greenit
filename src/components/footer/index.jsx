import './index.css';

const Footer = () => {

    return (
        <footer>
            <div className="footer-flex">
                <div className="footer-responsive">
                    <div className="footer-network">
                        <p className="footer-network-title">SUIVEZ-NOUS</p>
                        <div className="footer-links-network">
                            <div>
                                <div className="footer-flex-logo">
                                    <img src="/footer/facebook.avif" className="facebook-logo"/> 
                                    <a href=""><p>Facebook</p></a>
                                </div>
                                <div className="footer-flex-logo">
                                    <img src="/footer/twitter.avif" className="twitter-logo"/>
                                    <a href=""><p>Twitter</p></a>
                                </div>
                                <div className="footer-flex-logo">
                                    <img src="/footer/youtube.avif" className="youtube-logo"/>
                                    <a href=""><p>Youtube</p></a>
                                </div>
                            </div>
                            <div>
                                <div className="footer-flex-logo">
                                    <img src="/footer/instagram.avif" className="instagram-logo"/>
                                    <a href=""><p>Instagram</p></a>
                                </div>
                                <div className="footer-flex-logo">
                                    <img src="/footer/linkedin.avif" className="linkedin-logo"/>
                                    <a href=""><p>Linkedin</p></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="footer-contact">
                        <p className="footer-contact-title">ASSISTANCE JAGUAR</p>
                        <div className="footer-links-contact">
                            <a href=""><p>Contactez l'assitance</p></a>
                            <a href=""><p>Contactez le service client</p></a>
                            <a href=""><p>Contactez un concessionaire</p></a>
                        </div>
                    </div>
                </div>
                <div className="footer-responsive">
                    <div className="footer-contact">
                        <p className="footer-contact-title">À PROPOS DE JAGUAR</p>
                        <div className="footer-links-contact">
                            <a href=""><p>Découvrez Jaguar</p></a>
                            <a href=""><p>Véhicules</p></a>
                            <a href=""><p>Notre boutique</p></a>
                        </div>
                    </div>
                    <div className="footer-contact">
                        <p className="footer-contact-title">SERVICES ET ENTRETIEN</p>
                        <div className="footer-links-contact">
                            <a href=""><p>En savoir plus</p></a>
                            <a href=""><p>Manuels et guides</p></a>
                            <a href=""><p>Trouvez un concessionaire</p></a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    )
}
  
export default Footer;
  