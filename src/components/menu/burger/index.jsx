import './index.css';
import { Link } from 'react-router-dom';
import { menuItems } from '../menuItems';
import { data, xeSvProject8 } from '../../pages/SVO/data';

const Burger = () => {

    const listItems = menuItems[0].submenu.map((menu, index) =>
        <li key={index}><a href={menu.url}>{menu.title}</a></li>
    );

    const listItems2 = data.map((menu1, index) =>
        <li key={index}><Link to={`/cars/${menu1.id}`} state={{ from: menu1}}>{menu1.title}</Link></li>
    )

    const listItems3 = xeSvProject8.map((menu2, index) => 
        <li key={index}><Link to={`/cars/${menu2.id}`} state={{ from: menu2}}>{menu2.title}</Link></li>
    )

    return (
        <div className="burger">
            <nav role="navigation">
                <div id="menuToggle">
                
                    <input type="checkbox" />
                    
                    <span></span>
                    <span></span>
                    <span></span>
                    
                    <ul id="menu">
                        <a href="/"><li>Accueil</li></a>

                        <label htmlFor="menu1" id="competition" className="accordion">
                            <p>Véhicules</p>
                            <img src="/arrow2.png" className="submenu-arrow"/>
                        </label>
                        <input id="menu1" name="menu" type="checkbox" />

                        <ul className="sub" id="sub">
                            {listItems}
                        </ul>

                        <label htmlFor="menu3" id="competition" className="accordion">
                            <p>Votre Jaguar</p>
                            <img src="/arrow2.png" className="submenu-arrow"/>
                        </label>
                        <input id="menu3" name="menu" type="checkbox" />

                        <ul className="sub" id="sub">
                            <li><a href="/guides">Manuels et Guides</a></li>
                        </ul>

                        <label htmlFor="menu2" id="competition" className="accordion">
                            <p>Véhicules Spéciaux</p>
                            <img src="/arrow2.png" className="submenu-arrow"/>
                        </label>
                        <input id="menu2" name="menu" type="checkbox" />

                        <ul className="sub" id="sub">
                            <li><a href="/svo">Présentation</a></li>
                            {listItems2}
                            {listItems3}
                        </ul>
                    </ul>
                </div>
            </nav>

            <img src="/logo.avif" className="logoBurger"/>
        </div>
    )
}
  
export default Burger;
  