import Navbar from './Navbar'

const Header = () => {
  return (
    <header>
      <div className="nav-area">
        <a href="/" className="logo">
          <img src="/logo.avif" className="logoimg"/>
        </a>
        <Navbar />
      </div>
    </header>
  );
};

export default Header