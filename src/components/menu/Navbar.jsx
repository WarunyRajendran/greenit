import { menuItems } from '../menu/menuItems'
import Items from './Items'

const Navbar = () => {
  return (
    <nav>
      <ul className="menus">
        {menuItems.map((menu, index) => {
          return <Items items={menu} key={index} />
        })}
      </ul>
    </nav>
  );
};

export default Navbar