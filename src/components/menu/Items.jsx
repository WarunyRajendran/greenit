import { useState } from 'react';
import Dropdown from './Dropdown'

const Items = ({ items }) => {
  const [dropdown, setDropdown] = useState(false);
  return (
    <li className="menu-items">
      {items.submenu ? (
        <>
          <button 
            type="button" 
            aria-haspopup="menu" 
            aria-expanded={dropdown ? "true" : "false"}
            onClick={() => setDropdown((previousState) => !previousState)}
          >
            {items.title}{' '}
          </button>
          <Dropdown 
            submenus={items.submenu} 
            dropdown={dropdown}
            title={items.title}
          />
        </>
      ) : (
        <a href={items.url}>{items.title}</a>
      )}
    </li>
  );
  };
  
export default Items
  