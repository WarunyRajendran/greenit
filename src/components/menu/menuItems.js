export const menuItems = [
    {
        title: 'VÉHICULES',
        submenu: [
            {
                title: 'E-PACE',
                url: '/e-pace',
            },
            {
                title: 'F-PACE',
                url: '/f-pace',
            },
            {
                title: 'I-PACE',
                url: '/i-pace',
            },
            {
                title: 'XE',
                url: '/xe',
            },
            {
                title: 'XF',
                url: '/xf',
            },
            {
                title: 'F-TYPE',
                url: '/f-type',
            }
        ],
    },
    {
        title: 'VOTRE JAGUAR',
        submenu: [
            {
                title: 'Manuels et Guides',
                url: '/guides'
            },
        ],
    },
    {
        title: 'VÉHICULES SPÉCIAUX',
        submenu: [
            {
                title: 'Présentation',
                url: '/svo'
            },
            {
                title: 'F-Pace SVR 1988',
                url: '/cars/30', 
            },
            {
                title: 'F-Pace SVR',
                url: '/cars/31',
            },
            {
                title: 'XE SV Project 8',
                url: '/cars/32',
            },
        ],
    },
];