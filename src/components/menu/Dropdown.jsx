import classNames from 'classnames/bind';
import { Link } from 'react-router-dom';
import { data, xeSvProject8 } from '../pages/SVO/data';

const Dropdown = ({ submenus, dropdown, title }) => {

    return (
      <ul className={classNames('dropdown',{
        'show': dropdown,
      })}>
        
        {title === 'VÉHICULES SPÉCIAUX' ?
          <>
            <li className="menu-items">
              <Link to="/svo" state={{ from: data }}>Présentation</Link>
            </li>
            {data.map((data, index) => (
              <li key={index} className="menu-items">
                <Link to={`/cars/${data.id}`} state={{ from: data }}>{data.title}</Link>
              </li>
            ))}
            {xeSvProject8.map((data, index) => (
              <li key={index} className="menu-items">
                <Link to={`/cars/${data.id}`} state={{ from: data }}>{data.title}</Link>
              </li>
            ))}
          </>
        :
          submenus.map((submenu, index) => (
            <li key={index} className="menu-items">
              <Link to={submenu.url}>{submenu.title}</Link>
            </li>
          ))
        }
      </ul>
    );
};
  
export default Dropdown
  