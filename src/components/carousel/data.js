export const dataCarousel = [
    {
        name: 'epace',
        title: 'Jaguar E-Pace',
        image: './home/carousel/e-pace-carousel.avif',
        link: '/e-pace'
    },
    {
        name: 'ftype',
        title: 'Jaguar F-Type',
        image: './home/carousel/f-type-carousel.avif',
        link: '/f-type'
    },
    {
        name: 'xe',
        title: 'Jaguar XE',
        image: './home/carousel/xe-carousel.avif',
        link: '/xe'
    },
    {
        name: 'xf',
        title: 'Jaguar XF',
        image: './home/carousel/xf-carousel.avif',
        link: '/xf'
    }
];