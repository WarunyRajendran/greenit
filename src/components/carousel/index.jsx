import { dataCarousel } from './data';
import './index.css';

const Carousel = () => {

    return (
      <div className="carousel">
        <form>
          {dataCarousel.map((data, key) => {
            return (
              <input type="radio" name="fancy" autoFocus value={data.name} id={data.name} key={key}/>
            );
          })}

          {dataCarousel.map((data, key) => {
            return (
              <label htmlFor={data.name} key={key}>
                <div className="carousel-label-container">
                  <div className="carousel-label-container-text">
                    <p>{data.title}</p>
                    <a href={data.link}>En savoir plus</a>
                  </div>
                  <img src={data.image} className="carousel-image" loading="lazy"/>
                </div>
              </label>
            );
          })}
        </form>
      </div>
    )
}
  
export default Carousel;
  