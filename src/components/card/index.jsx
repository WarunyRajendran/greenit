import React from 'react';
import { Link } from 'react-router-dom';
import carData from './carData.json';
import './index.css';

function CarCard({ car }) {
    return (
        <div className="vehicle-card">
            <div className="details">
                <div className="thumb-gallery">
                    <img className="first" src={car.first_image} alt={car.model} loading="lazy" />
                    <img className="second" src={car.second_image} alt={car.model} loading="lazy" />
                </div>
                <div className='info'>
                    <h3>{car.model}</h3>
                    <div className="price">
                        <span>Prix à partir de:</span>
                        <h4>{car.price}€</h4>
                    </div>
                    <div className="ctas">
                        <Link className='btn primary' to={`/cars/${car.id}`} state={{ from: car }}>
                            Détails du modèle
                        </Link>
                        <div style={{ clear: 'both' }}></div>
                    </div>
                    <div className="desc">
                        <p>{car.desc}</p>
                    </div>
                    <div className="specs">
                        <div className="spec mpg">
                            <span>Conso</span>
                            <p>{car.consumption}<span>L/km</span></p>
                        </div>
                        <div className="spec mpg">
                            <span>0-100km</span>
                            <p>{car.acceleration}<span>sec</span></p>
                        </div>
                        <div className="spec mpg">
                            <span>Puissance</span>
                            <p>{car.horsepower}<span>ch</span></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

function CarList({ category }) {
    const filteredCars = carData.filter(car => car.category === category);

    return (
        <div className="wrapper">
            <h1 className='title-categories'>Jaguar {category}</h1>
            {filteredCars.map((car) => (
                <CarCard key={car.id} car={car} />
            ))}
        </div>
    );
}

export default CarList;
